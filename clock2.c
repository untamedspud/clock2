#include <stdio.h>
#include <stdlib.h>

#define ERROR_IN_TIME 0
#define ERROR_IN_INPUT 1

#define CLOCK_IS_12_HOUR 1
#define CLOCK_IS_24_HOUR 2

int add_hour( const int h, const int clock_type );

int main()
{
	int i;
	int h;

	for( i = 1; i < 14; i++ ) {
		h = add_hour( i, CLOCK_IS_12_HOUR );
		if ( h == ERROR_IN_TIME )
			printf( "The hour entered for add_hour is invalid as %d.\n", i );
		else
			printf( "add_hour(%d) = %d.\n", i, h );
	}

	for( i = 0; i < 24; i++ ) {
		h = add_hour( i, CLOCK_IS_24_HOUR );
		if ( h == ERROR_IN_TIME )
			printf( "The hour entered for add_hour is invalid as %d.\n", i );
		else
			printf( "add_hour(%d) = %d.\n", i, h );
	}

	return EXIT_SUCCESS;
}

int add_hour( const int h, const int clock_type )
{
	if ( h < 1 || h > 12 ) {
        #ifdef DEBUG
		    puts( "That value is invalid." );
        #endif
		return ERROR_IN_TIME;
	}

	return ( h < 12 ) ? ++h : 1;
}